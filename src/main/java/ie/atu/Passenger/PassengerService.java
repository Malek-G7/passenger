package ie.atu.Passenger;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Service

@Getter
@Setter
public class PassengerService {

    public List<Passenger> getPassengers(){
        List<Passenger> passengers = List.of(
        new Passenger("Mr","Malek","g000387032","0830593772",22),
        new Passenger("Mrs","Malek","g000387032","0830593772",22),
        new Passenger("Ms","Malek","g000387032","0830593772",22)
        );
        return passengers;
    }

}
